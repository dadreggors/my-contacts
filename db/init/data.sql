DROP TABLE IF EXISTS contacts_api.contacts;
DROP SCHEMA IF EXISTS contacts_api;
CREATE SCHEMA contacts_api;
CREATE TABLE contacts_api.contacts (
	id SERIAL PRIMARY KEY,
	fname	VARCHAR(20) NOT NULL,
	lname	VARCHAR(20) NOT NULL,
	phone	VARCHAR(12),
	email	VARCHAR(50),
	city	VARCHAR(20),
	state	VARCHAR(2),
	zip	VARCHAR(5)
);
INSERT INTO contacts_api.contacts VALUES(1,'Don','Johnson','555-555-0905','djohnson@gmail.com','Boca Raton','FL','33427');
INSERT INTO contacts_api.contacts VALUES(2,'Dick','Dickerson','555-555-0906','ddickerson@gmail.com','Boca Raton','FL','33431');
INSERT INTO contacts_api.contacts VALUES(3,'Richard','Cranium','555-555-0907','dick@gmail.com','Boca Raton','FL','33433');
INSERT INTO contacts_api.contacts VALUES(4,'Jerky','McJerkins','555-555-0908','jmcjerk','Boca Raton','FL','33427');
INSERT INTO contacts_api.contacts VALUES(5,'Jill','Munroe','555-555-0909','FarrahFawcett@charliesangels.com','Boca Raton','FL','33427');
INSERT INTO contacts_api.contacts VALUES(6,'Sabrina','Duncan','555-555-0910','KateJackson@charliesangels.com','Boca Raton','FL','33427');
INSERT INTO contacts_api.contacts VALUES(7,'Kelly','Garrett','555-555-0911','JaclynSmith@charliesangels.com','Boca Raton','FL','33427');

BEGIN;
-- protect against concurrent inserts while you update the counter
LOCK TABLE contacts_api.contacts IN EXCLUSIVE MODE;
-- Update the sequence
SELECT setval('contacts_api.contacts_id_seq', COALESCE((SELECT MAX(id)+1 FROM contacts_api.contacts), 1), false);
COMMIT;
