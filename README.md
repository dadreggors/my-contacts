# Docker Compose Demo

## The demo application
This demo contains two containers:
1. A simple micro-service layer written in go that provides a REST api to POST, GET, DELETE contacts
2. A Simple php web application that consumes the JSON from the micro-service api to display contacts
3. A Postgres database to house the data that the go micro-service layer uses

### Interesting parts
* I have implemented full CRUD in the go api and in the php so you can fully test the api
* I use a postgres container for the backend data for the go app
* I use the php:apache image directly from docker hub as is and mount volumes to be able to change the php on the fly and see changes without rebuilding the image

Why are these parts interesting? Because it allows you to see how to do these things and allows you to easily play with the code/data. You can learn with a hands on approach and see how a modular approach is useful. It also teaches the value of decoupling applications into micro-services vs building a monolithic application. In this fashion you can have several teams working independently to build the application faster and more robust, while also allowing for faster recovery if one component goes down.

## The Go contact micro-service container
I chose Go for a couple of reasons. The first is, I like Go. The second is that Go is fast and very light weight.
Another nice side effect of using Go was the ability to show how to build a slim image with a compiled app that runs fast.

Yes, I could have used Express/Ember/Angular (JS), Flask (Python) or Rails (Ruby)... but I wanted small, fast, and lightweight. No frameworks, no interpreter, no requirement for external binaries (Python, Ruby, etc..).  

All code for the contacts micro-service application and docker image can be seen under `contacts`.
There is a `Dockerfile` that instructs the build in docker-compose, and a `src/contacts_api` folder with the sql to build the
database and the go code to compile the app. All of which happen in the docker build.

## The PHP web frontend
I am quite sure there are tons of people out there that HATE php and will be miffed that I used it in this demo. Let them be mad,
php is mature and I know how to write a very small app (2 pages with no large framework) to consume json.

Sure I could so the same in JavaScript (Ember. Express, Angular, etc...), Python (DJango), or Ruby (Rails) but these are larger frameworks.
I wanted a very small, 2 or 3 page demo app that is not bogged down in css, libs, js, etc...

Also, and here is arguably the better point... this demo is for YOU to try and change this and learn from it. How about you build a NEW container using your frontend of choice and get rid of that offensive php? ;-)


The code for the php is located under `web/src` and the rest is configured in the `docker-compose.yml` file. Feel free modify the php or remove the entire container.
