<?php include_once("header.php"); ?>
<div class="page-header">
  <h1>Contacts</h1>
</div>
<div class="row">
  <div class="col-md-12">
    <table class="table">
      <thead>
        <tr>
          <th>Name</th>
          <th>Phone</th>
          <th>Email</th>
          <th>City</th>
          <th>State</th>
          <th>Zip</th>
        </tr>
      </thead>
      <tbody>
          <?php
            $base_url = "http://contacts:12355/people";
            $json = file_get_contents($base_url);
            $obj = json_decode($json,true);
            foreach($obj as $contact) {
              echo "<tr>";
              echo "<div class=\"row\">\n";
              $full_name = $contact["firstname"] . " " . $contact["lastname"];
              $contact_url = "http://localhost:5000/contact.php?id=" . $contact["id"];
              echo "<td><a href=\"$contact_url\">$full_name</a></td>";
              echo "<td>" . $contact["phone"] . "</td>";
              echo "<td>" . $contact["email"] . "</td>";
              echo "<td>" . $contact["city"] . "</td>";
              echo "<td>" . $contact["state"] . "</td>";
              echo "<td>" . $contact["zip"] . "</td>";
              echo "</div>";
              echo "</tr>";
            }
          ?>
      </tbody>
    </table>
  </div>
</div>
<a class="btn btn-primary" href="http://localhost:5000/contact.php">Add new contact</a>
<?php include_once("footer.php"); ?>
