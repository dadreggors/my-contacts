<?php include_once("header.php"); ?>
<div class="page-header">
  <h3>Contact Details</h3>
  <h5><a href="http://localhost:5000/">Back</a><h5>
</div>
<?php
  $base_url = "http://contacts:12355/people";
  $data = array(
    "id" => $_REQUEST["id"],
    "firstname" => htmlspecialchars($_REQUEST["firstname"]),
    "lastname" => htmlspecialchars($_REQUEST["lastname"]),
    "phone" => htmlspecialchars($_REQUEST["phone"]),
    "email" => htmlspecialchars($_REQUEST["email"]),
    "city" => htmlspecialchars($_REQUEST["city"]),
    "state" => htmlspecialchars($_REQUEST["state"]),
    "zip" => htmlspecialchars($_REQUEST["zip"])
  );
  if (isset($_REQUEST["add"]) && $_REQUEST["add"] != "") {
    unset($data["id"]);
    $data_string = json_encode($data);
    $curl = $base_url;
    $result = file_get_contents($curl, null, stream_context_create(array(
      'http' => array(
      'method' => 'POST',
      'header' => 'Content-Type: application/json' . "\r\n"
      . 'Content-Length: ' . strlen($data_string) . "\r\n",
      'content' => $data_string,
      ),
    )));
    if ($result !== false) {
      echo "<div class=\"alert alert-success\" role=\"alert\">Contact Added Successfully</div>";
    } else {
      echo "<div class=\"alert alert-danger\" role=\"alert\">Contact Addition Failed</div>";
    }
  } elseif (isset($_REQUEST["update"]) && $_REQUEST["update"] != "") {
    $data_string = json_encode($data);
    $curl = $base_url . "/" . $_REQUEST["id"];
    $result = file_get_contents($curl, null, stream_context_create(array(
      'http' => array(
      'method' => 'POST',
      'header' => 'Content-Type: application/json' . "\r\n"
      . 'Content-Length: ' . strlen($data_string) . "\r\n",
      'content' => $data_string,
      ),
    )));
    if ($result !== false) {
      echo "<div class=\"alert alert-success\" role=\"alert\">Contact Updated Successfully</div>";
    } else {
      echo "<div class=\"alert alert-danger\" role=\"alert\">Contact Update Failed</div>";
    }
  } elseif (isset($_REQUEST["delete"]) && $_REQUEST["delete"] != "") {
    $data_string = json_encode($data);
    $curl = $base_url . "/" . $_REQUEST["id"];
    $result = file_get_contents($curl, null, stream_context_create(array(
      'http' => array(
      'method' => 'DELETE',
      ),
    )));
    if ($result !== false) {
      echo "<div class=\"alert alert-success\" role=\"alert\">Contact Deleted Successfully</div>";
    } else {
      echo "<div class=\"alert alert-danger\" role=\"alert\">Contact Deletion Failed</div>";
    }
  } else {
    if (isset($data["id"]) && $data["id"] !== "" ) {
      $contact_url = $base_url . "/" . $data["id"];
    }else{
      $contact_url = $base_url;
    }
    $json = file_get_contents($contact_url);
    $obj = json_decode($json,true);
  }

?>
<div class="has-danger">
  <form method="post" action="contact.php">
    <input type="hidden" name="id" value="<?php echo $data["id"]; ?>">
    <div class="form-group">
      <label for="txtFirstName">First Name</label>
      <input type="text" name="firstname" class="form-control" id="txtFirstName" value="<?php echo $obj["firstname"] ?>">
    </div>
    <div class="form-group">
      <label for="txtLastName">Last Name</label>
      <input type="text" name="lastname" class="form-control" id="txtLastName" value="<?php echo $obj["lastname"] ?>">
    </div>
    <div class="form-group">
      <label for="txtPhone">Phone</label>
      <input type="phone" name="phone" class="form-control bfh-phone" data-format="+1 (ddd) ddd-dddd" id="txtPhone" value="<?php echo $obj["phone"] ?>">
    </div>
    <div class="form-group">
      <label for="txtEmail">Email address</label>
      <input type="email" name="email" class="form-control" id="txtEmail" aria-describedby="emailHelp" value="<?php echo $obj["email"] ?>">
    </div>
    <div class="form-group">
      <label for="txtCity">City</label>
      <input type="text" name="city" class="form-control" id="txtCity" value="<?php echo $obj["city"] ?>">
    </div>
    <div class="form-group">
      <label for="txtState">State</label>
      <input type="text" name="state" class="form-control" id="txtState" value="<?php echo $obj["state"] ?>">
    </div>
    <div class="form-group">
      <label for="txtZip">Zip</label>
      <input type="text" name="zip" class="form-control" id="txtZip" value="<?php echo $obj["zip"] ?>">
    </div>
    <?php
    if (isset($data["id"]) && $data["id"] !== "") {
      echo "<button type=\"submit\" name=\"update\" class=\"btn btn-primary\" value=\"update\">Update</button>\t";
      echo "<button type=\"submit\" name=\"delete\" class=\"btn btn-primary\" value=\"delete\">Delete</button>";
    }else{
      echo "<button type=\"submit\" name=\"add\" class=\"btn btn-primary\" value=\"add\">Add</button>";
    }
    ?>
  </form>
</div>
<?php include_once("footer.php"); ?>
