package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
)

// Person : Create a struct for the Person type objects
type Person struct {
	ID        string `json:"id,omitempty"`
	Firstname string `json:"firstname,omitempty"`
	Lastname  string `json:"lastname,omitempty"`
	Phone     string `json:"phone,omitempty"`
	Email     string `json:"email,omitempty"`
	City      string `json:"city,omitempty"`
	State     string `json:"state,omitempty"`
	Zip       string `json:"zip,omitempty"`
}

// dbInfo : A const defining the DB connection info
const (
	dbInfo = "host=db user=postgres dbname=contacts_api sslmode=disable"
	dbType = "postgres"
)

var (
	people   = make(map[string]Person)
	logfile  = "contact_api.log"
	loglevel = "INFO"
)

// Datebase Actions
// Here we define the functions that perform
// actions on the database.

// CheckDB : Poll for database readiness before we exec queries/statements
func CheckDB(db *sql.DB) {
	applog("Is DB Ready?", loglevel)
	for {
		count, cnxerr := db.Query("SELECT COUNT(id) FROM contacts_api.contacts")
		if cnxerr != nil {
			applog("no", loglevel)
			time.Sleep(2 * time.Second)
		} else {
			applog("yes", loglevel)
			defer count.Close()
			break
		}
	}
}

// GetPeople : Get contacts (people) from database
func GetPeople() {
	people = make(map[string]Person)
	var count int
	db, err := sql.Open(dbType, dbInfo)
	if err != nil {
		msg := "Error connecting to the database!"
		fmt.Println(msg)
		applog(msg, "TRACE")
	}
	err = db.Ping()
	if err != nil {
		msg := "Error: Could not establish a connection with the database"
		fmt.Println(msg)
		applog(msg, "TRACE")
	}
	defer db.Close()
	CheckDB(db)
	rows, err := db.Query("SELECT * FROM contacts_api.contacts")
	checkErr(err)
	rc := db.QueryRow("SELECT COUNT(id) FROM contacts_api.contacts")
	rcerr := rc.Scan(&count)
	checkErr(rcerr)
	var id string
	var fname string
	var lname string
	var phone string
	var email string
	var city string
	var state string
	var zip string
	for rows.Next() {
		err = rows.Scan(&id, &fname, &lname, &phone, &email, &city, &state, &zip)
		checkErr(err)
		people[id] = Person{ID: id, Firstname: fname, Lastname: lname, Phone: phone, Email: email, City: city, State: state, Zip: zip}
	}
	defer rows.Close()
	applog(strconv.Itoa(count)+" contacts found", loglevel)
}

// CreatePerson : Add a contact (person) to the database
func CreatePerson(fname string, lname string, phone string, email string, city string, state string, zip string) {
	db, err := sql.Open(dbType, dbInfo)
	checkErr(err)
	defer db.Close()
	CheckDB(db)
	sqlStmt := `
	insert
	into
	contacts_api.contacts(id,fname,lname,phone,email,city,state,zip)
	values(DEFAULT,$1,$2,$3,$4,$5,$6,$7)
	`
	stmt, err := db.Prepare(sqlStmt)
	checkErr(err)
	res, err := stmt.Exec(fname, lname, phone, email, city, state, zip)
	checkErr(err)
	rows, err := res.RowsAffected()
	checkErr(err)
	applog(string(rows)+" row(s) inserted", loglevel)
	defer stmt.Close()
	applog("Contact '"+fname+" "+lname+"' was added", loglevel)
	GetPeople()
}

// DeletePerson : Delete a contact (person) from the database
func DeletePerson(id string) {
	db, err := sql.Open(dbType, dbInfo)
	checkErr(err)
	defer db.Close()
	CheckDB(db)
	sqlStmt := `delete from contacts_api.contacts where id = $1`
	stmt, err := db.Prepare(sqlStmt)
	checkErr(err)
	res, err := stmt.Exec(id)
	checkErr(err)
	affect, err := res.RowsAffected()
	checkErr(err)
	stmt.Close()
	applog("Deleting contact '"+id+"'", loglevel)
	applog(strconv.FormatInt(affect, 10)+" rows affected", loglevel)
	GetPeople()
}

// UpdatePerson : Update the contact (person) in the database
func UpdatePerson(id string, fname string, lname string, phone string, email string, city string, state string, zip string) {
	db, err := sql.Open(dbType, dbInfo)
	checkErr(err)
	defer db.Close()
	CheckDB(db)
	sqlStmt := `
	update contacts_api.contacts
	set fname = $1,
	lname = $2,
	phone = $3,
	email = $4,
	city = $5,
	state = $6,
	zip = $7
	where id = $8
	`
	stmt, err := db.Prepare(sqlStmt)
	checkErr(err)
	res, err := stmt.Exec(fname, lname, phone, email, city, state, zip, id)
	checkErr(err)
	affect, err := res.RowsAffected()
	checkErr(err)
	stmt.Close()
	applog("Updating contact '"+id+"'", loglevel)
	applog(strconv.FormatInt(affect, 10)+" rows affected", loglevel)
	GetPeople()
}

// Endpoint Actions
// Here we define the functions that perform
// actions on requests to our routed endpoints.

// GetPersonEndpoint : Create an endpoint for getting Person objects
func GetPersonEndpoint(w http.ResponseWriter, req *http.Request) {
	params := mux.Vars(req)
	var id = params["id"]
	person, exists := people[id]
	if exists == true {
		applog("Found '1' contact", loglevel)
		applog("Displaying contact '"+id+"'", loglevel)
	} else {
		applog("Contact '"+id+"' not found", loglevel)
		applog("Displaying empty set", loglevel)
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(person)
}

// GetPeopleEndpoint : Create an endpoint for multiple Person objects
func GetPeopleEndpoint(w http.ResponseWriter, req *http.Request) {
	GetPeople()
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(people)
}

// CreatePersonEndpoint : Create an endpoint for create person objects
func CreatePersonEndpoint(w http.ResponseWriter, req *http.Request) {
	p := Person{}
	err := json.NewDecoder(req.Body).Decode(&p)
	checkErr(err)

	CreatePerson(p.Firstname, p.Lastname, p.Phone, p.Email, p.City, p.State, p.Zip)
	GetPeople()
}

// DeletePersonEndpoint : Create an endpoint to delete Person objects
func DeletePersonEndpoint(w http.ResponseWriter, req *http.Request) {
	params := mux.Vars(req)
	id, _ := params["id"]
	DeletePerson(id)
	delete(people, id)
}

// UpdatePersonEndpoint : Update the contact (person)
func UpdatePersonEndpoint(w http.ResponseWriter, req *http.Request) {
	p := Person{}
	err := json.NewDecoder(req.Body).Decode(&p)
	checkErr(err)

	UpdatePerson(p.ID, p.Firstname, p.Lastname, p.Phone, p.Email, p.City, p.State, p.Zip)
}

// GetIndexEndpoint : Create an endpoint for the index (/)
func GetIndexEndpoint(w http.ResponseWriter, req *http.Request) {
	json.NewEncoder(w).Encode("Bad Request")
}

// Utility Functions
// Here we define the functions that perform
// actions to support database or endpoint function calls.

// checkErr : Check for error and panic if found
func checkErr(err error) {
	if err != nil {
		applog(err.Error(), "TRACE")
		panic(err)
	}
}

// applog : Logging messages to a logfile
func applog(msg string, level string) {
	file, _ := os.OpenFile(logfile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0644)
	var logger *log.Logger
	logger = log.New(file, "["+level+"] ", log.Ldate|log.Ltime)
	logger.Println(msg)
	file.Close()
}

// Main
// Here we define the function that
// is the main entry point to the service.
//
// This function sets up the routes and sets
// up the initial environment.

// main : The main entry point in the contact_api app
func main() {
	applog("Starting", loglevel)
	router := mux.NewRouter()
	GetPeople()
	applog("Server listening on http://localhost:12355/", loglevel)
	applog("Available Endpoints:", loglevel)
	applog("\t/people - List contacts (GET)", loglevel)
	applog("\t/people - Create contact (POST)", loglevel)
	applog("\t/people/{id} - View, update, or delete contacts (Determined by GET, POST, or DELETE methods)", loglevel)

	router.HandleFunc("/people", GetPeopleEndpoint).Methods("GET")
	router.HandleFunc("/people", CreatePersonEndpoint).Methods("POST")
	router.HandleFunc("/people/{id}", GetPersonEndpoint).Methods("GET")
	router.HandleFunc("/people/{id}", UpdatePersonEndpoint).Methods("POST")
	router.HandleFunc("/people/{id}", DeletePersonEndpoint).Methods("DELETE")
	router.HandleFunc("/", GetIndexEndpoint).Methods("GET")
	http.ListenAndServe(":12355", router)
}
